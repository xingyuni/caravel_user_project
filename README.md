# Caravel Analog User

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![CI](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml) [![Caravan Build](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml)

---

| :exclamation: Important Note            |
|-----------------------------------------|

## Please fill in your project documentation in this README.md file 
1. verilog/rtl/macro.v, verilog/rtl/bgr_proj.v, verilog/rtl/user_analog_proj_wrapper.v
2. xschem/user_analog_project_wrapper.sch
3. mag/bgr_top_flat.mag
4. Next step: user_analog_project_wrapper manually layout


:warning: | Use this sample project for analog user projects. 
:---: | :---

---

Refer to [README](docs/source/index.rst) for this sample project documentation. 

